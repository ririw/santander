import joblib
from fs import appfs, zipfs

cache_fs = appfs.UserCacheFS("santander")  # pylint: disable=C0103
_DATA_LOC = "/Users/riri/Datasets/santander-customer-transaction-prediction.zip"
data_fs = zipfs.ZipFS(_DATA_LOC)  # pylint: disable=C0103
cache = joblib.Memory()  # pylint: disable=C0103
