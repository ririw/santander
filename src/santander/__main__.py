import logging

from plumbum import cli
import coloredlogs

import santander as s


class Santander(cli.Application):  # pylint: disable=too-few-public-methods
    debug = cli.Flag("-d")

    def main(self):  # pylint: disable=no-self-use
        if self.debug:
            lvl = logging.DEBUG
        else:
            lvl = logging.WARN

        coloredlogs.install(level=lvl)
        s.modelling.fit_model()


def main():
    return Santander.run()


if __name__ == "__main__":
    main()
