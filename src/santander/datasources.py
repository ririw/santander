import pickle

import pandas as pd
import numpy as np
import santander as s


def train_data():
    if not s.cfg.cache_fs.exists("train_data.npy"):
        with s.cfg.data_fs.open("train.csv", "r") as f:
            res = pd.read_csv(f, index_col="ID_code").astype(np.float32)
        with s.cfg.cache_fs.open("train_data.meta.pkl", "wb") as f:
            pickle.dump(res.index, f)
            pickle.dump(res.columns, f)

        with s.cfg.cache_fs.open("train_data.tmp.npy", "wb") as f:
            np.save(f, res.values)

        s.cfg.cache_fs.move("train_data.tmp.npy", "train_data.npy")

    data = np.load(s.cfg.cache_fs.getsyspath("train_data.npy"), mmap_mode="c")
    res = pd.DataFrame(data)
    with s.cfg.cache_fs.open("train_data.meta.pkl", "rb") as f:
        res.index = pickle.load(f)
        res.columns = pickle.load(f)

    return res
