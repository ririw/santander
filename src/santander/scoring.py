from sklearn import metrics

kaggle_metric = metrics.roc_auc_score  # pylint: disable=C0103
kaggle_scorer = metrics.make_scorer(  # pylint: disable=C0103
    kaggle_metric, needs_proba=True
)
