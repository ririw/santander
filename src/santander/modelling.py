import logging

import lightgbm
import numpy as np
import pandas as pd
import plumbum.colors
from sklearn import base, model_selection, pipeline, tree, preprocessing
from tqdm import tqdm

import santander as s


def ident(x):
    return x


def fit_model() -> base.BaseEstimator:
    dataset = s.datasources.train_data()
    model = construct_model()
    x = dataset.drop("target", 1)
    y = dataset["target"]
    shuffle_splitter = model_selection.ShuffleSplit(
        n_splits=3, test_size=0.1, random_state=8888
    )
    verbosity = logging.getLogger().level <= logging.INFO
    if verbosity:
        logging_rate = 25
    else:
        logging_rate = 0

    score = model_selection.cross_val_score(
        model,
        x,
        y,
        scoring=s.scoring.kaggle_scorer,
        cv=shuffle_splitter,
        fit_params={
            "autostoppinglightgbm__early_stopping_rounds": 25,
            "autostoppinglightgbm__verbose": logging_rate,
        },
    )
    # noinspection PyUnresolvedReferences
    print(plumbum.colors.green | "Scores: {}".format(score))  # pylint: disable=E1101

    with open("./model-benchmark.txt", "w") as f:
        f.write("Scores: {}\n".format(score))

    return model


def construct_model():
    return pipeline.make_pipeline(
        SyntheticRemover(),
        RetainColsOver(preprocessing.StandardScaler()),
        PDFeatureUnion.make_union(CountFeature(), Ident()),
        AutoStoppingLightGBM(
            lightgbm.LGBMClassifier(
                n_estimators=1024, learning_rate=0.05, num_leaves=15
            ),
            cross_validator=model_selection.ShuffleSplit(test_size=0.1),
        ),
    )


class Ident(base.BaseEstimator, base.TransformerMixin):
    def fit(self, _, __=None):
        return self

    @staticmethod
    def transform(x):
        return x


class CountFeature(base.BaseEstimator, base.TransformerMixin):
    def __init__(self):
        self.count_mappings = None

    def fit(self, x, _=None):
        self.count_mappings = {}
        for col in x.columns:
            col_vals: pd.Series = x[col]
            count_mapping = {}
            for round_level in range(1, 4):
                rounded_val = col_vals.round(round_level + 1)
                val_counts = rounded_val.value_counts()
                count_mapping[round_level] = val_counts
            self.count_mappings[col] = count_mapping
        return self

    def transform(self, x):
        res_cols = []
        for col in x.columns:
            for rount_level, mapper in self.count_mappings[col].items():
                new_col = x[col].map(mapper).rename("{}-c-{}".format(col, rount_level))
                res_cols.append(new_col)

        return pd.concat(res_cols, 1)


class RetainColsOver(base.BaseEstimator, base.TransformerMixin):
    def __init__(self, inner_est):
        self.inner_est = inner_est
        self._cols = None

    def fit(self, x, y):
        self._cols = x.columns.tolist()
        self.inner_est.fit(x, y)
        return self

    def transform(self, x):
        assert x.columns.tolist() == self._cols
        return pd.DataFrame(
            self.inner_est.transform(x), index=x.index, columns=self._cols
        )


class JointIndependentModels(base.BaseEstimator, base.TransformerMixin):
    def __init__(self):
        self._base_estimator = tree.DecisionTreeClassifier()
        self._estimators = {}

    def fit(self, x, y) -> "JointIndependentModels":
        assert x.columns.size == len(set(x.columns))
        for col in x:
            new_est = base.clone(self._base_estimator)
            new_est.fit(x[[col]], y)
            self._estimators[col] = new_est
        return self

    def predict_proba(self, x) -> np.ndarray:
        all_ests = []
        for col, est in self._estimators.items():
            pred = est.predict_proba(x[[col]])
            all_ests.append(pred)

        res = np.mean(all_ests, 0)
        msg = "Expected shape ({},2), got {}".format(x.shape[0], res.shape)
        assert res.shape == (x.shape[0], 2), msg
        # return pd.DataFrame(res, index=x.index, columns=[0, 1])
        return res

    def predict(self, x) -> pd.Series:
        return self.predict_proba(x)[:, 1] > 0.5


class SyntheticRemover(base.BaseEstimator, base.TransformerMixin):
    def __init__(self):
        self._synthetic_cols = None

    def fit(self, x_vals, _=None):
        self._synthetic_cols = []
        for col in x_vals:
            if x_vals[col].nunique() == x_vals[col].size:
                self._synthetic_cols.append(col)
        return self

    def transform(self, x_vals):
        assert self._synthetic_cols is not None
        return x_vals.drop(self._synthetic_cols, 1)


class Uniqueness(base.BaseEstimator, base.TransformerMixin):
    def __init__(self, val_encoding=None, as_int=False, no_bar=False):
        self.no_bar = no_bar
        self.as_int = as_int
        if val_encoding is None:
            val_encoding = {}
        self.val_encoding = val_encoding

    def fit(self, x_vals, y_vals):
        cat_ix = ["unique", "many-pos", "many-neg", "multi"]
        cat_dtype = pd.Categorical(cat_ix)
        no_bar = logging.getLogger().level > logging.INFO
        no_bar |= self.no_bar
        for col in tqdm(x_vals.columns, disable=no_bar):
            target_count = y_vals.groupby(x_vals[col]).value_counts().unstack()
            has_another_pos = target_count[1] >= 2
            has_another_neg = target_count[0] >= 2
            has_a_pos = target_count[1] >= 1
            has_a_neg = target_count[0] >= 1
            feat_mapper = pd.Series("unique", target_count.index, dtype=np.dtype("O"))

            feat_mapper[has_a_pos & has_a_neg] = "multi"
            feat_mapper[has_another_pos & ~has_a_neg] = "many-pos"
            feat_mapper[has_another_neg & ~has_a_pos] = "many-neg"
            self.val_encoding[col] = feat_mapper.astype(cat_dtype)

        return self

    def transform(self, x_vals):
        cat_ix = ["unique", "many-pos", "many-neg", "multi"]
        cat_dtype = pd.Categorical(cat_ix)
        res = []
        no_bar = logging.getLogger().level > logging.INFO
        no_bar |= self.no_bar
        for col in tqdm(x_vals.columns, disable=no_bar):
            mapped = (
                x_vals[col]
                .map(self.val_encoding[col])
                .fillna("unique")
                .astype(cat_dtype)
            )
            if self.as_int:
                mapped = mapped.cat.codes
            mapped.name = "unique-{}".format(col)
            res.append(mapped)

        return pd.concat(res, 1, sort=False)


class PDFeatureUnion(base.BaseEstimator, base.TransformerMixin):
    @classmethod
    def make_union(cls, *transform):
        return cls(transform)

    def __init__(self, transforms):
        self.transforms = transforms
        self.fit = s.cfg.cache.cache(self.fit)  # pylint: disable=method-hidden
        self.transform = s.cfg.cache.cache(
            self.transform
        )  # pylint: disable=method-hidden

    def fit(self, x, y=None):
        for trf in self.transforms:
            trf.fit(x, y)
        return self

    def transform(self, x):
        trf_sets = []
        for trf in self.transforms:
            trf_sets.append(trf.transform(x))

        res = pd.concat(trf_sets, 1, sort=False)
        msg = "Expected shape {} but got {}".format(res.shape[0], x.shape[0])
        assert res.shape[0] == x.shape[0], msg
        return res


class AutoStoppingLightGBM(base.BaseEstimator, base.ClassifierMixin):
    def __init__(self, estimator=None, cross_validator=None):
        self.cross_validator: model_selection.BaseCrossValidator = cross_validator
        self.estimator: lightgbm.LGBMClassifier = estimator

    def fit(self, x, y=None, **fit_args):
        train, stop = next(self.cross_validator.split(x, y))
        self.estimator.fit(
            x.iloc[train],
            y.iloc[train],
            eval_set=[(x.iloc[stop], y.iloc[stop])],
            **fit_args
        )
        return self

    def predict_proba(self, x):
        return self.estimator.predict_proba(x)

    def predict(self, x):
        return self.estimator.predict(x)
