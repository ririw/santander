"""santander - Kaggle stuff"""

from santander import cfg
from santander import datasources
from santander import scoring
from santander import modelling

__all__ = ["cfg", "datasources", "scoring", "modelling"]

__version__ = "0.1.0"
__author__ = "Richard Weiss <richardweiss@richardweiss.org>"
