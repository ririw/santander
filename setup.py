import io
import os
import re

from setuptools import find_packages
from setuptools import setup


def read(filename):
    filename = os.path.join(os.path.dirname(__file__), filename)
    text_type = type(u"")
    with io.open(filename, mode="r", encoding="utf-8") as fhandle:
        return re.sub(
            text_type(r":[a-z]+:`~?(.*?)`"), text_type(r"``\1``"), fhandle.read()
        )


setup(
    name="santander",
    version="0.1.0",
    url="https://gitlab.com/ririw/santander",
    license="MIT",
    author="Richard Weiss",
    author_email="richardweiss@richardweiss.org",
    description="Kaggle stuff",
    long_description=read("README.rst"),
    packages=find_packages(exclude=("tests",)),
    package_dir={"": "src"},
    install_requires=[
        "plumbum",
        "scikit-learn",
        "lightgbm",
        "joblib",
        "fs",
        "numpy",
        "pandas",
        "coloredlogs",
        "tqdm",
    ],
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    entry_points={"console_scripts": ["santander = santander.__main__:main"]},
)
